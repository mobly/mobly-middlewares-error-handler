# @mobly/middlewares/error-handler

Middleware de tratamento de erros

## Instalação

Para instalar o pacote:

```
npm i @moblybr/middlewares-error-handler
```

Para usar este middleware, importe-o no arquivo que for utilizá-lo:

```javascript

const errorHandler = require('@moblybr/middlewares-error-handler')

```

## Middleware Middy para Error Handler

Captura, trata e customiza erros na aplicação.   
Repositório: https://bitbucket.org/mobly/mobly-middlewares-error-handler  
Recomendamos uso do Middleware Error Handler na primeira/última posição da pilha de middlewares Middy.

#### Customização de erros
O Middleware vem com configurações básicas para repassar erros HTTP e montar erro 500 para as exceções não tratadas.   
A aplicação pode customizar livremente o tratamento de erros HTTP e não-HTTP e sobrescrever esses padrões.
O Middleware espera um objeto com ate 5 elementos: customHttpErrors, customLogger e customNonHttpErrors, customDlqErrors, explodeErrors.

#### CustomHttpErrors
É usado para customizar erros HTTP da aplicação. O objeto deve conter chave com o error.name padrão HTTP, statusCode e body com propriedade message. Por exemplo:
```javascript
{
  InternalServerError: {
    statusCode: 500,
    body: { message: 'Erro interno de servidor' }
  },
  ForbiddenError: {
    statusCode: 403,
    body: { message: 'Acesso negado' }
  },
}
```

#### CustomHttpErrors com processamento customizado
Além de responder um erro estático é possível também receber o erro em uma função e manipulá-lo livremente. O objeto deve conter chave com o error.name padrão HTTP, função process e retornar um objeto com statusCode e body com propriedade message. Por exemplo:
```javascript
{
  BadRequestError: {
    process (e) {
      if (e.message === 'Event object failed validation') {
        const messages = e.details.map(detail => detail.message)
        return {
          statusCode: 400,
          body: { message: messages.join(', ') }
        }
      }
      return {
        statusCode: 400,
        body: { message: e.message || 'Requisição mal formada' }
      }
    }
  }
}
```

#### CustomLogger
Como padrão o Middleware fazer um console.error como log. Para customizar o logger é possível passsar uma função. Por exemplo:
```javascript
import Log from '@dazn/lambda-powertools-logger'
import epsagon from 'epsagon'

(err) => {
  Log.error(err.message, { date: new Date() }, err)
  epsagon.setError(err)
}
```

#### customNonHttpErrors
É possível manipular erros não-HTTP através da função `customNonHttpErrors`. Ela deve retornar um objeto com statusCode e body com propriedade message. Por exemplo:
```javascript
(err) => {
  console.error(err)
  return { InternalServerError: { statusCode: 500, body: { message: 'Internal Server Error' } } }
}
```

#### customDlqErrors
E possivel tambem configurar uma fila sqs dlq para enviar erros selecionados através de um objeto chamado `customDlqErrors`, para isso é necessário dois parâmetros específicos `typeErrors` em forma de um `array` de objetos contendo as propriedades `statusCode` com o numero do codigo de erro ou uma string `'noHttp'` para erros não http e `url` com a url da fila sqs específica de envio do erro que deseja enviar para a dlq, podemos também passar `urlDefault` propriedade para setar uma url de uma fila sqs padrão para os status codes que não forem expecificados uma url de destino. Por exemplo:

##### Importante
E necessario passar ao menos uma url de uma fila sqs para poder ser enviado, e é necessário as permissões iam adequadas para a lambda que ira realizar o envio para o dlq.


```javascript
const customDlqErrors = {
  urlDefault: 'uri padrão',
  typeErrors: [
    {statusCode: 400, url: 'http://xxx'}
    {statusCode: 'noHttp'} // ele enviara qualquer erro não http para a urlDefault config
    ]
}

```
#### explodeErrors
Esse é um parametro para explodir os erros e não responder eles tratado. Exemplo:

```javascript
explodeErrors: true
```

##### Importante 
Quando se tratar de um evento sqs a lamda retorna o erro para a mesma fila sqs por padrão, para realizar as tentativas de retry antes de estourar o erro para a fila dlq.

#### Exemplo de código
Os handlers do boiler plate usam o Middleware Error Handler com exemplo de configurações customizadas