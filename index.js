const { sendDlq } = require('./src/sendDlq')
const { isCustomError, nonHTTPError } = require('./src/httpErrors')

const defaults = {
  customHttpErrors: {
    InternalServerError: {
      statusCode: 500,
      body: { message: 'Internal Server Error' }
    }
  },
  customLogger: () => console.error,
  customDlqErrors: {},
  explodeErrors: false
}

const errorHandlerMiddleware = (opts = {}) => {
  // sobrescrevendo defaults com configurações customizadas
  const options = {}
  options.customLogger =
    typeof opts.customLogger === 'function'
      ? opts.customLogger
      : defaults.customLogger

  options.customHttpErrors = {
    ...defaults.customHttpErrors,
    ...opts.customHttpErrors
  }
  options.customDlqErrors = {
    ...defaults.customDlqErrors,
    ...opts.customDlqErrors
  }

  options.explodeErrors = opts.explodeErrors || defaults.explodeErrors
  options.customNonHttpErrors = opts.customNonHttpErrors

  const errorHandlerMiddlewareOnError = async (request) => {
    // verifica se existe configurações para envio de erros para dlq
    if (typeof opts.customDlqErrors === 'object') {
      await sendDlq(request, options.customDlqErrors)
    }
    // verifica se é erro http
    request.response =
      request.error.statusCode && request.error.message
        ? isCustomError(request, options.customHttpErrors)
        : nonHTTPError(request, options)

    // logger
    options.customLogger(request)

    // trata body
    request.response.body =
      typeof request.response.body !== 'string'
        ? JSON.stringify(request.response.body)
        : request.response.body

    if (options.explodeErrors) throw request.error
  }

  return {
    onError: errorHandlerMiddlewareOnError
  }
}

module.exports = errorHandlerMiddleware
