// verifica se tem erros customizados
const isCustomError = (request, customHttpErrors) => {
  const err = request.error
  return customHttpErrors[err.name]
    ? isCustomProcess(request, customHttpErrors[err.name])
    : isStandardError(request.error, customHttpErrors)
}

// verifica se tem processamento de erro customizado
const isCustomProcess = (request, customError) =>
  typeof customError.process === 'function'
    ? customError.process(request)
    : customError

// monta erro http padrão
const isStandardError = (err, customHttpErrors) => {
  const statusCode = customHttpErrors.statusCode || err.statusCode
  let body = { message: err.message }

  body = customHttpErrors.body || body
  return { statusCode, body }
}

// veririca se tem erros não http customizados
const nonHTTPError = (request, options) =>
  typeof options.customNonHttpErrors === 'function'
    ? options.customNonHttpErrors(request)
    : genericError(options.customHttpErrors)

// monta erro genérico para erro não tratado
const genericError = customHttpErrors => customHttpErrors.InternalServerError

module.exports = { isCustomError, nonHTTPError }
