const { SQSClient, SendMessageCommand } = require('@aws-sdk/client-sqs')

// monta o params para envio do dlq, valida se a uri existe e chama o metodo
// que mapeia erros a serem enviados pra dlq
const sendDlq = async (request, config) => {
  if (Array.isArray(config.typeErrors)) {
    return mapStatusCode(request, config)
  }
}

// mapeia os status codes enviados e faz o envio pra dlq
const mapStatusCode = async (request, config) => {
  const clientSqs = new SQSClient({
    region: config.awsRegion || 'us-east-1'
  })
  return Promise.all(
    config.typeErrors.map(async code => {
      const { statusCode } = request.error

      if (!(code.url || config.urlDefault)) {
        throw new Error('Nenhuma url passada como parametro')
      }

      if (code.statusCode === statusCode || code.statusCode === 'noHttp') {
        const params = {
          MessageBody: JSON.stringify(request),
          QueueUrl: code.url || config.urlDefault
        }
        const command = new SendMessageCommand(params)
        return clientSqs.send(command)
      }
    })
  )
}

module.exports = { sendDlq }
