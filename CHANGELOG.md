# [2.1.0](https://bitbucket.org/mobly/mobly-middlewares-error-handler/compare/v2.0.0...v2.1.0) (2019-12-03)


### Features

* **sqs-dlq:** Feature/sqs dead letter queue ([8f13f05](https://bitbucket.org/mobly/mobly-middlewares-error-handler/commits/8f13f05))

# [2.0.0](https://bitbucket.org/mobly/mobly-middlewares-error-handler/compare/v1.0.1...v2.0.0) (2019-11-19)


* feat!: Atualizando pacote para passar o handler completo para poder ser capturado ([cf33835](https://bitbucket.org/mobly/mobly-middlewares-error-handler/commits/cf33835))


### BREAKING CHANGES

* Mudando forma que o parametro é passado

## [1.0.1](https://bitbucket.org/mobly/mobly-middlewares-error-handler/compare/v1.0.0...v1.0.1) (2019-10-31)


### Bug Fixes

* **doc:** Update Readme with npm ([78cbd54](https://bitbucket.org/mobly/mobly-middlewares-error-handler/commits/78cbd54))

# 1.0.0 (2019-10-31)


### Bug Fixes

* **onError:** fixing parameters ([ee78b17](https://bitbucket.org/mobly/mobly-middlewares-error-handler/commits/ee78b17)), closes [#1](https://bitbucket.org/mobly/mobly-middlewares-error-handler/issue/1) [#2](https://bitbucket.org/mobly/mobly-middlewares-error-handler/issue/2) [#1](https://bitbucket.org/mobly/mobly-middlewares-error-handler/issue/1) [#2](https://bitbucket.org/mobly/mobly-middlewares-error-handler/issue/2)
* **onError:** fixing parameters ([4f12b58](https://bitbucket.org/mobly/mobly-middlewares-error-handler/commits/4f12b58)), closes [#1](https://bitbucket.org/mobly/mobly-middlewares-error-handler/issue/1) [#2](https://bitbucket.org/mobly/mobly-middlewares-error-handler/issue/2) [#1](https://bitbucket.org/mobly/mobly-middlewares-error-handler/issue/1) [#2](https://bitbucket.org/mobly/mobly-middlewares-error-handler/issue/2)
* **onError:** fixing parameters ([b3e3f9e](https://bitbucket.org/mobly/mobly-middlewares-error-handler/commits/b3e3f9e)), closes [#1](https://bitbucket.org/mobly/mobly-middlewares-error-handler/issue/1) [#2](https://bitbucket.org/mobly/mobly-middlewares-error-handler/issue/2)
* **onError:** fixing parameters ([2e3b903](https://bitbucket.org/mobly/mobly-middlewares-error-handler/commits/2e3b903)), closes [#1](https://bitbucket.org/mobly/mobly-middlewares-error-handler/issue/1) [#2](https://bitbucket.org/mobly/mobly-middlewares-error-handler/issue/2)


### chore

* **master:** First release to master ([609ea0b](https://bitbucket.org/mobly/mobly-middlewares-error-handler/commits/609ea0b)), closes [#1](https://bitbucket.org/mobly/mobly-middlewares-error-handler/issue/1) [#2](https://bitbucket.org/mobly/mobly-middlewares-error-handler/issue/2)
* **pipeline:** Adding resources to semantic version bump and npm publish ([f8edeaa](https://bitbucket.org/mobly/mobly-middlewares-error-handler/commits/f8edeaa))


### BREAKING CHANGES

* **pipeline:** First push
* **onError:** Adding first bump with this commit to master
* **onError:** Adding first bump with this commit to master

    * fix(onError): fixing parameters
* **onError:** Adding first bump with this commit to master
* **onError:** Adding first bump with this commit to master
* **onError:** Adding first bump with this commit to master

    * fix(onError): fixing parameters
* **onError:** Adding first bump with this commit to master
* **onError:** Adding first bump with this commit to master
* **master:** Adding first bump with this commit to master
* **onError:** Adding first bump with this commit to master
