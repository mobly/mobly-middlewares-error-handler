const createError = require('http-errors')
const middy = require('@middy/core')
const errorHandler = require('../index')

const context = {
  getRemainingTimeInMillis: () => 1000
}

describe('Error Handler', () => {
  it('success - standard http error', async () => {
    const handler = middy(() => {
      throw createError.NotFound()
    })
    handler.use(errorHandler())
    const response = await handler(null, context)

    const body = JSON.parse(response.body)
    await expect(response.statusCode).toBe(404)
    await expect(body.message).toBe('Not Found')
  })

  it('success - custom http error', async () => {
    const customHttpErrors = {
      NotFoundError: {
        statusCode: 404,
        body: { message: 'Digital object not found' }
      }
    }

    const handler = middy(() => {
      throw createError.NotFound()
    })
    handler.use(errorHandler({ customHttpErrors }))
    const response = await handler(null, context)

    const body = JSON.parse(response.body)
    await expect(response.statusCode).toBe(404)
    await expect(body.message).toBe('Digital object not found')
  })

  it('success - custom processed http error', async () => {
    const customHttpErrors = {
      NotFoundError: {
        process: handler => {
          return {
            statusCode: 404,
            body: { message: handler.error.message.replace(/ /gi, '_') }
          }
        }
      }
    }

    const handler = middy(() => {
      throw createError.NotFound()
    })
    handler.use(errorHandler({ customHttpErrors }))
    const response = await handler(null, context)

    const body = JSON.parse(response.body)
    await expect(response.statusCode).toBe(404)
    await expect(body.message).toBe('Not_Found')
  })

  it('success - generic error', async () => {
    const handler = middy(() => {
      throw new Error('Unexpected unidentified error')
    })
    handler.use(errorHandler())
    const response = await handler(null, context)

    const body = JSON.parse(response.body)
    await expect(response.statusCode).toBe(500)
    await expect(body.message).toBe('Internal Server Error')
  })

  it('success - non http error', async () => {
    const customNonHttpErrors = () => {
      return {
        statusCode: 500,
        body: { message: 'Something went very very wrong' }
      }
    }

    const handler = middy(() => {
      throw new Error('Unexpected unidentified error')
    })
    handler.use(errorHandler({ customNonHttpErrors }))
    const response = await handler(null, context)

    const body = JSON.parse(response.body)
    await expect(response.statusCode).toBe(500)
    await expect(body.message).toBe('Something went very very wrong')
  })

  it('explode errors', async () => {
    const handler = middy(() => {
      throw createError.NotFound()
    })
    handler.use(errorHandler({ explodeErrors: true }))

    try {
      await handler(null, context)
    } catch (err) {
      expect(err).toHaveProperty('message')
      expect(err.message).toBe('Not Found')
    }
  })

  it('test custom logger is function', async () => {
    const customLogger = (request) => {
      expect(request.error.message).toBe('Not Found')
    }
    const handler = middy(() => {
      throw createError.NotFound()
    })
    handler.use(errorHandler({ customLogger }))
    const response = await handler(null, context)

    const body = JSON.parse(response.body)
    await expect(response.statusCode).toBe(404)
    await expect(body.message).toBe('Not Found')
  })

  it('test opts not send', async () => {
    const handler = middy(() => {
      throw createError.NotFound()
    })
    handler.use(errorHandler())
    const response = await handler(null, context)

    const body = JSON.parse(response.body)
    await expect(response.statusCode).toBe(404)
    await expect(body.message).toBe('Not Found')
  })

  it('body is string and customDlqErrors is not object', async () => {
    const handler = middy(() => {
      throw createError.NotFound()
    })

    const customDlqErrors = 'ola mundo'
    const customHttpErrors = {
      NotFoundError: {
        process: (request) => {
          return {
            statusCode: request.error.statusCode,
            body: JSON.stringify({
              message: 'Not Found'
            })
          }
        }
      }
    }

    handler.use(errorHandler({ customHttpErrors, customDlqErrors }))
    const response = await handler(null, context)

    const body = JSON.parse(response.body)
    await expect(body.message).toBe('Not Found')
    await expect(response.statusCode).toBe(404)
  })

  it('test parameter typeErrors to customDlqErrors is not array', async () => {
    const handler = middy(() => {
      throw createError.NotFound()
    })

    const customDlqErrors = {
      urlDefault: '127.0.0.1',
      typeErrors: {}
    }

    handler.use(errorHandler({ customDlqErrors }))
    const response = await handler(null, context)

    const body = JSON.parse(response.body)
    await expect(body.message).toBe('Not Found')
    await expect(response.statusCode).toBe(404)
  })

  it('test noHttp config', async () => {
    const handler = middy(() => {
      throw createError.NotFound()
    })

    const customDlqErrors = {
      urlDefault: '127.0.0.1',
      typeErrors: [{ statusCode: 'noHttp' }]
    }

    handler.use(errorHandler({ customDlqErrors }))
    // vai disparar um error porque a fila não existe
    await expect(handler(null, context)).rejects.toThrow()
  })

  it('test url is not send in config', async () => {
    const handler = middy(() => {
      throw createError.NotFound()
    })

    const customDlqErrors = {
      typeErrors: [{ statusCode: 404 }]
    }

    handler.use(errorHandler({ customDlqErrors }))
    await expect(handler(null, context)).rejects.toThrow('Nenhuma url passada como parametro')
  })

  it('test handler.error.statusCode not equal statusCode config', async () => {
    const handler = middy(() => {
      throw createError.NotFound()
    })

    const customDlqErrors = {
      typeErrors: [{ statusCode: 500, url: '127.0.0.1' }]
    }
    handler.use(errorHandler({ customDlqErrors }))
    const response = await handler(null, context)

    const body = JSON.parse(response.body)
    await expect(body.message).toBe('Not Found')
    await expect(response.statusCode).toBe(404)
  })
})
